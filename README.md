# My Gitlab code of conduct

## Create repo

Create a repository we need some informations like:
- Programming language
- All the name and description in English language
- The aims of the project
- The logo 
- The README.md file with the simplest info about this repo
- Add into the README.md file the sentence : "The project is valid at the time of its publication" at the beginning
- *Optional INSTALL.md to install
- *French language if explicitly mentionned in README file

### Name of the Repository

How to compose the name of my repositories.  
Before that some abbreviation of the technologies and other use :
```
k : kubernetes
py : python
b: bash script
d : docker
di : docker images 
fl : flask
doc : documentation
cu : custom
```

Exemple : for a project which need to construct a docker image for specific project named _gitpod_ : `gitpod-cu-di`


### How to make a commit 

Need to learn this : https://www.conventionalcommits.org/en/v1.0.0/  
And a custom abbrev.

### Necessary files in

- The directory of the logo with some format
- the gitlab-ci file to check the build of [commit messages](https://www.conventionalcommits.org/en/v1.0.0/)


## Links :
- https://www.conventionalcommits.org/en/v1.0.0/
